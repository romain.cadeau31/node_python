import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserJsonService {

  public dataUser: any[] = [];

  constructor(private httpClient: HttpClient, private router: Router) { }

  onSubmitSearchForm(form: NgForm): void {
    const firstname = form.value['firstname'];
    const lastname = form.value['lastname'];
    this.httpClient
      .get<any>('http://localhost:3000/name?firstname=' + firstname + '&lastname=' + lastname)
      .subscribe({
        next: (result: any) => {
          this.dataUser = result.data;
        },
        error: (err: any) => {
          console.log(err);
        },
        complete: () => {
        console.log('complete');
        this.router.navigate(['/Search-Candidate-Result']);
        }
        });
  }

}
