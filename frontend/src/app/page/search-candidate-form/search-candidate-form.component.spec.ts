import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCandidateFormComponent } from './search-candidate-form.component';

describe('SearchCandidateFormComponent', () => {
  let component: SearchCandidateFormComponent;
  let fixture: ComponentFixture<SearchCandidateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchCandidateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCandidateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
