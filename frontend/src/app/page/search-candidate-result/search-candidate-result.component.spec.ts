import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCandidateResultComponent } from './search-candidate-result.component';

describe('SearchCandidateResultComponent', () => {
  let component: SearchCandidateResultComponent;
  let fixture: ComponentFixture<SearchCandidateResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchCandidateResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCandidateResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
