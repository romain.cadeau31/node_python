import { Component, OnInit } from '@angular/core';
import { UserJsonService } from '../../user-json.service';

@Component({
  selector: 'app-search-candidate-result',
  templateUrl: './search-candidate-result.component.html',
  styleUrls: ['./search-candidate-result.component.scss']
})
export class SearchCandidateResultComponent implements OnInit {

  public dataUser: any[] = [];

  constructor(private userJsonService: UserJsonService ) { }

  ngOnInit(): void {
    this.dataUser = this.userJsonService.dataUser;
  }

}
