import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-my-radar-chart',
  templateUrl: './my-radar-chart.component.html',
  styleUrls: ['./my-radar-chart.component.scss']
})
export class MyRadarChartComponent implements OnInit {

  @Input() data: any;
  radarChartLabels: any[] = [];
  radarChartData: any[] = [];
  public radarChartType: any = 'radar';


  constructor() { }

  ngOnInit(): void {
    const radarTresBien: any = [];
    const radarBien: any = [];
    const radarAssezBien: any = [];
    const radarIncertain: any = [];
    const radarAucuneDonnees: any = [];
    this.radarChartLabels = this.data[1];
    for (const resL of this.data[1]) {
    radarTresBien.push(3);
    radarBien.push(2);
    radarAssezBien.push(1);
    radarIncertain.push(0.5);
    radarAucuneDonnees.push(0);
    }
    this.radarChartData.push({data: this.data[2], label: this.data[0]});
    this.radarChartData.push({data: radarTresBien, label: 'Très bien'});
    this.radarChartData.push({data: radarBien, label: 'Bien'});
    this.radarChartData.push({data: radarAssezBien, label: 'Assez bien'});
    this.radarChartData.push({data: radarIncertain, label: 'Incertain'});
    this.radarChartData.push({data: radarAucuneDonnees, label: 'Aucune données'});
  }

}
