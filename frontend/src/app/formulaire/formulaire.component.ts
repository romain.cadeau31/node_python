import { UserJsonService } from '../user-json.service';
import { Component, Injectable, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Data } from 'phaser';

import { from } from 'rxjs';

@Injectable()

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {

  constructor(private userJsonService: UserJsonService) { }

  ngOnInit(): void {

  }

  onSubmit(form: NgForm): void {
    this.userJsonService.onSubmitSearchForm(form);
  }


}
