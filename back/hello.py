import sys

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json


data = pd.read_csv('test.csv')

print(data.to_json(orient="split"))


# Takes first name and last name via command
# line arguments and then display them
#print("First name: " + sys.argv[1] + "Last name: " + sys.argv[2])


# save the script as hello.py
