# -*- coding: utf-8 -*-
"""
Created on Sun Apr 18 12:51:57 2021

@author: bruno
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import json


# create function---------------------------------------------------

# function filter statuts
def txfilter_status(x0, x, y):
    j = 0
    for i in x0:
        if x[j] != '':
            y = y[y[i].str.contains(x[j], regex=True) == True]
        j = j+1
    return y

# end function filter status

# function filter thematics


def txfilter_thematics(x, y_users, y_users_point):

    users_condtion = ''
    # create dataframe

    Thedata = []

    for i in y_users['_id']:
        users_condition = users_condtion+'|'+i

    y_users_point = y_users_point[y_users_point['userId'].str.contains(
        users_condition, regex=True) == True]
    y_users_point = y_users_point[y_users_point['category'].str.contains(
        x, regex=True) == True]

    for user in y_users_point['userId']:
        occurence_array = list(np.where(data_users_point['userId'] == user)[0])
        a0 = user
        keys = []
        cat = []
        finalpoints = []
        for k in occurence_array:
            cat.append(data_users_point['category'][k])
            keys.append(data_users_point['keywords'][k])
            nb_of_keywords = data_users_point['keywords'][k].count(",")+1
            # if not list(np.where(data_users_point['keywords'][k]==',')[0]):
            #   #print('je suis ici')
            #  nb_of_keywords=1
            # else:
            #     nb_of_keywords=list(np.where(data_users_point['keywords'][k]==',')[0])
            thepoints = []
            for i in range(nb_of_keywords):
                thepoints.append(data_users_point['points'][k])
            finalpoints.append(thepoints)
            # ite=data_users_point['keywords'][len(occurence_array)].count(',')+1
            # points.append(var_points)
        keys = str(keys)
        keys = keys.replace('[', '')
        keys = keys.replace(']', '')
        keys = keys.replace("'", '')
        keys = keys.split(',')
        finalpoints = str(finalpoints).replace(
            '[', '').replace(']', '').split(',')
        dico = {'identifiant': a0, 'category': cat,
                'keywords': keys, 'points': finalpoints}
        Thedata.append(dico)
    dtframe_users = pd.DataFrame(data=Thedata)
    # end
    return dtframe_users
# end function filter thematics

# function filter skills - to do later


def tx_skills(S1):
    gathering_data = []
    list_ref = []
    vecteur_score = []
    same_skills_point = []

    for i in range(len(S1['identifiant'])):  # i is the user
        # list of skills of i
        list_ref = list(dict.fromkeys(S1['keywords'][i]))
        # look inside each unique skills, interest only for the j-th
        for j in range(len(list_ref)):
            for m in range(len(S1['keywords'][i])):  # look inside all the skills

                # look for the m-th skills for the i-th user
                if list_ref[j] == S1['keywords'][i][m]:
                    # add the score if it is the j-th unique skills
                    same_skills_point.append(int(S1['points'][i][m]))

            vecteur_score_0 = [sum(same_skills_point), max(same_skills_point), min(same_skills_point), np.mean(
                same_skills_point), np.var(same_skills_point), np.std(same_skills_point)]
            vecteur_score.append(vecteur_score_0)
            same_skills_point = []
        # vecteur_score.append(vecteur_score_0)
        dico = {'identifiant': S1['identifiant'][i], 'category': S1['category'][i], 'keywords': str(
            list_ref).replace('[', '').replace(']', ''), 'keywordstableau': list_ref, 'vecteur_score': vecteur_score}
        gathering_data.append(dico)
        vecteur_score = []
        list_ref = []
    dtframe_users = pd.DataFrame(data=gathering_data)
    return dtframe_users


def txfilter_skills(x_skills, S2):
    S3 = S2[S2['keywords'].str.contains(x_skills, regex=True) == True]
    l = list(range(len(S3['identifiant'])))
    s = pd.Series(l)
    S3 = S3.set_index([s])
    return S3


def txranking(level_of_importance, x_skills, S3):
    list_x_skills = x_skills.split('|')
    tableau_score = []
    gathering_data = []
    scoring = []
    for i in range(len(S3['identifiant'])):
        # user=S3[S3['identifiant'].str.contains(i,regex=False)==True]
        for j in range(len(list_x_skills)):
            for k in range(len(S3['keywordstableau'][i])):
                #print(str(list_x_skills[j]).replace('[', '').replace(']', ''))
                # print(j)
                # print(S3['keywordstableau'][i][k])
                if str(list_x_skills[j]).replace('[', '').replace(']', '').strip() == S3['keywordstableau'][i][k].strip():
                    # print('okkkkk')
                    scoring.append(S3['vecteur_score'][i][k][3])
                else:
                    scoring.append(-1)
            tableau_score.append(max(scoring))
            scoring = []
        dico = {'identifiant': S3['identifiant'][i], 'liste_compétences': list_x_skills, 'score': np.array(
            tableau_score), 'cost_function': sum([x * y for x, y in zip(np.array(tableau_score), level_of_importance)])}
        gathering_data.append(dico)
        tableau_score = []
    dtframe_users = pd.DataFrame(data=gathering_data)
    dtframe_users.drop_duplicates(
        subset="identifiant", keep="first", inplace=True)
    l = list(range(len(dtframe_users['identifiant'])))
    s = pd.Series(l)
    dtframe_users = dtframe_users.sort_values(
        by='cost_function', ascending=False)
    dtframe_users = dtframe_users.set_index([s])
    return dtframe_users


# def cost_function(x1,x2,x3)
# end function skills - to do later

# function create ranking

# end function create ranking

# end create funciton--------------------------------------------

# load database***********************************************
data_users = pd.read_csv('users.csv')
data_users_point = pd.read_csv('user_ratings.csv')
data_users_point = data_users_point.dropna(subset=['keywords'])
# end load database*******************************************

# input from user//////////////////////////////////////////////////
var_x = ['nationality', 'zipcode', 'country', 'experience',
         'university', 'profileLink', 'isHandicapped']
x_status = ['', '', 'France|Martinique', '', '', '', '']
x_thematics = 'Interview oral|Bureautique'
x_skills = 'SK_english|SW_Ciel|SW_excel|hse'
level_of_importance = np.array([1, 1000, 100])
# end input from user/////////////////////////////////////////////////

# RUN SCRIPT
S = txfilter_status(var_x, x_status, data_users)
S1 = txfilter_thematics(x_thematics, S, data_users_point)
S2 = tx_skills(S1)
S3 = txfilter_skills(x_skills, S2)
S4 = txranking(level_of_importance, x_skills, S3)
print(S4.to_json(orient="split"))
# END RUN
