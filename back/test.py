import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json

# function filter statuts


def txfilter_status(x0, x, y):
    j = 0
    for i in x0:
        if x[j] != '':
            y = y[y[i].str.contains(x[j], regex=True) == True]
        j = j+1
    return y

# end function filter status


# input 1: csv
data = pd.read_csv('test.csv')
# fin input 1

# input 2: string
x1 = 'ichi|ni|san|yon|go'
# x1='ichi|ni|san|yon|go|roku|nana|hachi|kyu|ju'
# input 2

# input 3: list
var_x = ['Pays', 'Ville']
x_status = ['Inde|Martinique', 'Fort de France|Chenay']
# fin input 3

# script
S = txfilter_status(var_x, x_status, data)
S = S[S['Label'].str.contains(x1, regex=True) == True]
# end script

# Output
S.plot()
# fin output

#S.to_json(r'home/cadeau/Bureau/STAGE TXTEAM/node python/test/test.json', orient="split")
